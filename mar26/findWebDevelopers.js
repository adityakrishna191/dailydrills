const employeeDetails = require("./p1");

// 1. Find all Web Developers
const webDevs = (data) => {
    const result = data.filter(entry => {
        // console.log(entry.job);
        if(entry.job.includes("Web Developer")){
            // console.log(entry.job); 
            // console.log (`${entry.first_name} ${entry.last_name}`);
            // return `${entry.first_name} ${entry.last_name}`;
            return entry; //got stuck here tried to return full name here itself !!!
        }
    }).map(entry => {
        return `${entry.first_name} ${entry.last_name}`
    })
    return result;
}

console.log(webDevs(employeeDetails));
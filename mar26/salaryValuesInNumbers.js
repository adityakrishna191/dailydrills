const employeeDetails = require("./p1");


// 2.Convert all the salary values into proper numbers instead of strings 
const salaryValuesInNumbers = (data) => {
    data.forEach(entry => {
        entry.salary = Number(entry.salary.slice(1));
    })
}

module.exports = salaryValuesInNumbers;

salaryValuesInNumbers(employeeDetails);
// console.log(employeeDetails);
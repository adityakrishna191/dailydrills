const employeeDetails = require("./p1");
const salaryValuesInNumbers = require("./salaryValuesInNumbers");
const factoredSalary = require("./factoredSalary");

// 5. Find the sum of all salaries based on country using only HOF method
const sumOfSalariesBasedOnCountry = (data) => {
    const countryFilterSalaryObj = {}
    data.map(entry => {
        if(countryFilterSalaryObj[entry.location] === undefined) {
            countryFilterSalaryObj[entry.location] = entry.factoredSalary;
        }else{
            countryFilterSalaryObj[entry.location] += entry.factoredSalary;
        }
    })
    // console.log(countryFilterSalaryObj);
    return countryFilterSalaryObj;
}

module.exports = sumOfSalariesBasedOnCountry;

const result = sumOfSalariesBasedOnCountry(employeeDetails);
// console.log(result);
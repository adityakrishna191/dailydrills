const employeeDetails = require("./p1");
const salaryValuesInNumbers = require("./salaryValuesInNumbers");
const factoredSalary = require("./factoredSalary");

// 4. Find the sum of all salaries 
const sumOfAllSalaries = (data) => {
    // salaryValuesInNumbers(data);
    // factoredSalary(data);
    let sum = 0;
    const salaryData = data.map(entry => {
        return entry.factoredSalary;
    })
    // console.log(salaryData);
    sum = salaryData.reduce((sum,salary) => sum + salary);
    return sum;
}

const result = sumOfAllSalaries(employeeDetails);
console.log(result); //1463000
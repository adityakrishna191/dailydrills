const employeeDetails = require("./p1");
const salaryValuesInNumbers = require("./salaryValuesInNumbers");


// 3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)
const factoredSalary = (data) => {
    // salaryValuesInNumbers(data);

    data.forEach(entry => {
        entry.factoredSalary = entry.salary*10000
    })
}

module.exports = factoredSalary;

factoredSalary(employeeDetails)
// console.log(employeeDetails);

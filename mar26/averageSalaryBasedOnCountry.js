const employeeDetails = require("./p1");
const sumOfSalariesBasedOnCountry = require("./sumOfSalariesBasedOnCountry");


// 6. Find the average salary of based on country using only HOF method
const averageSalaryBasedOnCountry = (data) => {
    const countryCount = {};
    const salaryByCountry = sumOfSalariesBasedOnCountry(data);
    const avgSalaryByCountry = {};

    data.map(entry => {
        if(countryCount[entry.location] === undefined){
            countryCount[entry.location] = 1;
        }
        else{
            countryCount[entry.location]++;
        }
    })
    // console.log(countryCount);
    for(let country in salaryByCountry){
        avgSalaryByCountry[country] = salaryByCountry[country]/countryCount[country];
    }
    // console.log(avgSalaryByCountry);
    return avgSalaryByCountry;
}

const result = averageSalaryBasedOnCountry(employeeDetails);
console.log(result);
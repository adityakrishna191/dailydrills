const cardDetails = require("./p3");

// 3. Assign a new field to each card for CVV where the CVV is a random 3 digit number.

const generateCvv = () => {
    return ("" + Math.random()).substring(2, 5); // googled-up on how to generate random 3digit num
}

// console.log(generateCvv());

const generateAndAddRandomCvvNum = (data) => {
    data
        .map((detail) => {
            detail["CVV"] = generateCvv(); //task 3
            return detail;
        })
        .map((detail) => {
            detail["isValid"] = true; // task 4
            return detail;
        })
        .map((detail) => {
            if (detail.issue_date.split("/")[0] < 3) { // invalidate cards issued before March(3)
                detail.isValid = false;
            }
            return detail;
        })
    // data.sort((Number(detail.issue_date.split("/")[0]), ?) => )
    return data;
}

const result = generateAndAddRandomCvvNum(cardDetails);
console.log(result);
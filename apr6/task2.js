const cardDetails = require("./p3");

// 2. Find all cards that were issued before June.
const cardsIssuedBeforeJune = (data) => {
    return data.filter((details) => {
        const cardIssueDate = details.issue_date;
        const cardIssueMonth = cardIssueDate.split("/")[0];
        // console.log(cardIssueMonth);
        if(cardIssueMonth < 6){
            return details;
        }
    })
}

const result = cardsIssuedBeforeJune(cardDetails);
console.log(result);
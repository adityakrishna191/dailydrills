const cardDetails = require("./p3");

// 1. Find all card numbers whose sum of all the even position digits is odd.
const cardNumbersSumEvenPositionIsOdd = (data) => {
    return data.map((details) => {
        // return details.card_number.split("")
        const cardNums = details.card_number;
        const cardNumsSplit = details.card_number.split("");
        let sum = 0;
        cardNumsSplit.map((nums, index) => {
            // const cardNum = cardNum.split("");
            // let sum = 0;
            if(index%2===0){
                sum += Number(nums);                
                // return nums;
            }
            // console.log(sum);            
        });
        if(sum%2===1){
            return cardNums;
        }
        else{
            return "Sum of all even position digits is even";
        }        
    })
}

const result = cardNumbersSumEvenPositionIsOdd(cardDetails);
console.log(result);
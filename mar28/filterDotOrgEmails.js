const peopleDetails = require("./p2")

// 5. Filter out all the .org emails
const filterDotOrgEmail = (data) => {
    return data.filter(entry => {
        if (entry.email.includes(".org")){
            return entry;
        };
    }).map(entry => {
        return entry.email;
    })
}

const result = filterDotOrgEmail(peopleDetails);
console.log(result);
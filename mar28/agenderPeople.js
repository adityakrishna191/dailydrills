const peopleDetails = require("./p2");

// 1. Find all people who are Agender
const agenderPeople = (data) => {
    return data.filter(entry => {
        return (entry.gender === "Agender")
    }).map(entry => {
        return `${entry.first_name} ${entry.last_name}`;
    })
}

const result = agenderPeople(peopleDetails);
console.log(result);
const peopleDetails = require("./p2");

// 2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
const splitIpAddress = (data) => {
    const ipAddresses = data.map(entry => {
        return entry.ip_address;
    });
    // console.log(ipAddresses);
    const splitIp = ipAddresses.map(ipAddress => {
        return ipAddress.split(".");
    })
    // console.log(splitIp);
    return splitIp;
}

module.exports = splitIpAddress;

const result = splitIpAddress(peopleDetails);
// console.log(result);
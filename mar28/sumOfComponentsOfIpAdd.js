const peopleDetails = require("./p2");
const splitIpAddress = require("./splitIpAddressIntoComponents");

// 3. Find the sum of all the second components of the ip addresses.
const sumOf2ndComponent = (data) => {
    const splitAddresses = splitIpAddress(data);
    // console.log(splitAddresses);
    const secondComponent = splitAddresses.map((entry) => {
            return entry[1];
        }
    )
    // console.log(secondComponent);
    let sum = 0;
    sum = secondComponent.reduce((sum, entry) => {
        return sum + Number(entry);
    }, sum);
    // console.log(sum);
    return sum;
}

const result1 = sumOf2ndComponent(peopleDetails);
console.log(result1);

// 3. Find the sum of all the fourth components of the ip addresses.
const sumOf4thComponent = (data) => {
    const splitAddresses = splitIpAddress(data);
    // console.log(splitAddresses);
    const fourthComponent = splitAddresses.map((entry) => {
            return entry[3]; //this can be made modular, only diff from 2nd component is value of index;
        }
    )
    // console.log(fourthComponent);
    let sum = 0;
    sum = fourthComponent.reduce((sum, entry) => {
        return sum + Number(entry);
    }, sum);
    // console.log(sum);
    return sum;
}

const result2 = sumOf4thComponent(peopleDetails);
console.log(result2);
